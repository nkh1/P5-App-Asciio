# Asciio connectors

Connectors are a one-glyph-text elements, used by *git mode*, which can also be used to add connectors to user-created elements.

| Binding | Connector |
| ------- | --------- |
| o       | «ic»      |
| *       | «iC»      |

![Add Connector](add_connector.gif)


