package Asciio::BindingsHelp ;

our $bindings_help =
{
'Add angled arrow' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '00S-A',
	HELP => 'Add angled arrow',
	},

'Add arrow' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '000-a',
	HELP => 'Add arrow',
	},

'Add ascii line' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '000-l',
	HELP => 'Add ascii line',
	},

'Add ascii no-connect line' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '000-k',
	HELP => 'Add ascii no-connect line',
	},

'Add box' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '000-b',
	HELP => 'Add box',
	},

'Add connector' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '000-c',
	HELP => 'Add connector',
	},

'Add connector type 2' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '00S-C',
	HELP => 'Add connector type 2',
	},

'Add ellipse' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '000-e',
	HELP => 'Add ellipse',
	},

'Add exec box' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => 'C00-e',
	HELP => 'Add exec box',
	},

'Add exec box verbatim' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => 'C00-v',
	HELP => 'Add exec box verbatim',
	},

'Add exec box verbatim once' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => 'C00-o',
	HELP => 'Add exec box verbatim once',
	},

'Add help box' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => 'C00-h',
	HELP => 'Add help box',
	},

'Add horizontal ruler' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => 'C0S-R',
	HELP => 'Add horizontal ruler',
	},

'Add if' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '000-i',
	HELP => 'Add if',
	},

'Add line numbered box' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => 'C00-l',
	HELP => 'Add line numbered box',
	},

'Add multiple boxes' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => 'C00-b',
	HELP => 'Add multiple boxes',
	},

'Add multiple texts' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => 'C00-t',
	HELP => 'Add multiple texts',
	},

'Add process' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '000-p',
	HELP => 'Add process',
	},

'Add rhombus' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '000-r',
	HELP => 'Add rhombus',
	},

'Add shrink box' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '00S-B',
	HELP => 'Add shrink box',
	},

'Add text' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '000-t',
	HELP => 'Add text',
	},

'Add unicode angled arrow' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '0AS-A',
	HELP => 'Add unicode angled arrow',
	},

'Add unicode arrow' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '0A0-a',
	HELP => 'Add unicode arrow',
	},

'Add unicode bold line' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '0A0-l',
	HELP => 'Add unicode bold line',
	},

'Add unicode box' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '0A0-b',
	HELP => 'Add unicode box',
	},

'Add unicode double line' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '0AS-L',
	HELP => 'Add unicode double line',
	},

'Add unicode line' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '00S-L',
	HELP => 'Add unicode line',
	},

'Add unicode no-connect bold line' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '0A0-k',
	HELP => 'Add unicode no-connect bold line',
	},

'Add unicode no-connect double line' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '0AS-K',
	HELP => 'Add unicode no-connect double line',
	},

'Add unicode no-connect line' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => '00S-K',
	HELP => 'Add unicode no-connect line',
	},

'Add vertical ruler' => 
	{
	GROUP_NAME => 'Insert leader',
	SHORTCUTS => 'C00-r',
	HELP => 'Add vertical ruler',
	},

'Align bottom' => 
	{
	GROUP_NAME => 'align leader',
	SHORTCUTS => '000-b',
	HELP => 'Align bottom',
	},

'Align center' => 
	{
	GROUP_NAME => 'align leader',
	SHORTCUTS => '000-c',
	HELP => 'Align center',
	},

'Align left' => 
	{
	GROUP_NAME => 'align leader',
	SHORTCUTS => '000-l',
	HELP => 'Align left',
	},

'Align middle' => 
	{
	GROUP_NAME => 'align leader',
	SHORTCUTS => '000-m',
	HELP => 'Align middle',
	},

'Align right' => 
	{
	GROUP_NAME => 'align leader',
	SHORTCUTS => '000-r',
	HELP => 'Align right',
	},

'Align top' => 
	{
	GROUP_NAME => 'align leader',
	SHORTCUTS => '000-t',
	HELP => 'Align top',
	},

'Angled arrow context_menu' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'aa_ontext menu',
	HELP => 'Angled arrow context_menu',
	},

'Append multi_wirl section' => 
	{
	GROUP_NAME => 'arrow leader',
	SHORTCUTS => '000-s',
	HELP => 'Append multi_wirl section',
	},

'Arrow change direction' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'CA0-d',
	HELP => 'Arrow change direction',
	},

'Arrow mouse change direction' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'CA0-2button-press-1',
	HELP => 'Arrow mouse change direction',
	},

'Arrow to mouse' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'CA0-motion_notify',
	HELP => 'Arrow to mouse',
	},

'Asciio context_menu' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'as_context_menu',
	HELP => 'Asciio context_menu',
	},

'Box context_menu' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'bo_context_menu',
	HELP => 'Box context_menu',
	},

'Change Asciio background color' => 
	{
	GROUP_NAME => 'change color/font leader',
	SHORTCUTS => '0A0-c',
	HELP => 'Change Asciio background color',
	},

'Change arrow direction' => 
	{
	GROUP_NAME => 'arrow leader',
	SHORTCUTS => '000-d',
	HELP => 'Change arrow direction',
	},

'Change elements background color' => 
	{
	GROUP_NAME => 'change color/font leader',
	SHORTCUTS => '00S-C',
	HELP => 'Change elements background color',
	},

'Change elements foreground color' => 
	{
	GROUP_NAME => 'change color/font leader',
	SHORTCUTS => '000-c',
	HELP => 'Change elements foreground color',
	},

'Change font' => 
	{
	GROUP_NAME => 'change color/font leader',
	SHORTCUTS => '000-f',
	HELP => 'Change font',
	},

'Change grid color' => 
	{
	GROUP_NAME => 'change color/font leader',
	SHORTCUTS => '0AS-C',
	HELP => 'Change grid color',
	},

'Copy to clipboard' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  'C00-c',
  'C00-Insert',
  'y'
],
	HELP => 'Copy to clipboard',
	},

'Delete selected elements' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  '000-Delete',
  '000-d'
],
	HELP => 'Delete selected elements',
	},

'Deselect all elements' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-Escape',
	HELP => 'Deselect all elements',
	},

'Display action files' => 
	{
	GROUP_NAME => 'commands leader',
	SHORTCUTS => '000-f',
	HELP => 'Display action files',
	},

'Display commands' => 
	{
	GROUP_NAME => 'commands leader',
	SHORTCUTS => '000-c',
	HELP => 'Display commands',
	},

'Display keyboard mapping' => 
	{
	GROUP_NAME => 'commands leader',
	SHORTCUTS => '000-k',
	HELP => 'Display keyboard mapping',
	},

'Display manpage' => 
	{
	GROUP_NAME => 'commands leader',
	SHORTCUTS => '000-m',
	HELP => 'Display manpage',
	},

'Display numbered objects' => 
	{
	GROUP_NAME => 'debug leader',
	SHORTCUTS => '000-t',
	HELP => 'Display numbered objects',
	},

'Display undo stack statistics' => 
	{
	GROUP_NAME => 'debug leader',
	SHORTCUTS => '000-u',
	HELP => 'Display undo stack statistics',
	},

'Dump all elements' => 
	{
	GROUP_NAME => 'debug leader',
	SHORTCUTS => '000-e',
	HELP => 'Dump all elements',
	},

'Dump bindings' => 
	{
	GROUP_NAME => 'debug leader',
	SHORTCUTS => '000-b',
	HELP => 'Dump bindings',
	},

'Dump selected elements' => 
	{
	GROUP_NAME => 'debug leader',
	SHORTCUTS => '000-E',
	HELP => 'Dump selected elements',
	},

'Dump self' => 
	{
	GROUP_NAME => 'debug leader',
	SHORTCUTS => '000-s',
	HELP => 'Dump self',
	},

'Edit inline' => 
	{
	GROUP_NAME => 'change color/font leader',
	SHORTCUTS => '000-i',
	HELP => 'Edit inline',
	},

'Edit selected element' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  '000-2button-press-1',
  '000-Return'
],
	HELP => 'Edit selected element',
	},

'Edit selected element inline' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  'C00-2button-press-1',
  'C00-Return'
],
	HELP => 'Edit selected element inline',
	},

'Export to clipboard & primary as ascii' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  'C00-e',
  '00S-Y',
  'Y'
],
	HELP => 'Export to clipboard & primary as ascii',
	},

'Export to clipboard & primary as wiki' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'C0S-E',
	HELP => 'Export to clipboard & primary as wiki',
	},

'Flip arrow start and end' => 
	{
	GROUP_NAME => 'arrow leader',
	SHORTCUTS => '000-f',
	HELP => 'Flip arrow start and end',
	},

'Flip color scheme' => 
	{
	GROUP_NAME => 'change color/font leader',
	SHORTCUTS => '000-s',
	HELP => 'Flip color scheme',
	},

'Flip grid display' => 
	{
	GROUP_NAME => 'change color/font leader',
	SHORTCUTS => '000-g',
	HELP => 'Flip grid display',
	},

'Flip hint lines' => 
	{
	GROUP_NAME => 'change color/font leader',
	SHORTCUTS => '000-h',
	HELP => 'Flip hint lines',
	},

'Flip transparent element background' => 
	{
	GROUP_NAME => 'change color/font leader',
	SHORTCUTS => '000-t',
	HELP => 'Flip transparent element background',
	},

'Git add arrow' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => '000-a',
	HELP => 'Git add arrow',
	},

'Git add box' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => '000-b',
	HELP => 'Git add box',
	},

'Git add text' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => '000-t',
	HELP => 'Git add text',
	},

'Git change arrow direction' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => '000-d',
	HELP => 'Git change arrow direction',
	},

'Git delete elements' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => [
  '000-Delete',
  '000-x'
],
	HELP => 'Git delete elements',
	},

'Git edit selected element' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => [
  '000-2button-press-1',
  '000-Return'
],
	HELP => 'Git edit selected element',
	},

'Git flip hint lines' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => '000-h',
	HELP => 'Git flip hint lines',
	},

'Git mouse left-click' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => '000-button-press-1',
	HELP => 'Git mouse left-click',
	},

'Git mouse motion' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => '000-motion_notify',
	HELP => 'Git mouse motion',
	},

'Git mouse right-click' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => '0A0-button-press-3',
	HELP => 'Git mouse right-click',
	},

'Git move elements down' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => '000-Down',
	HELP => 'Git move elements down',
	},

'Git move elements left' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => '000-Left',
	HELP => 'Git move elements left',
	},

'Git move elements right' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => '000-Right',
	HELP => 'Git move elements right',
	},

'Git move elements up' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => '000-Up',
	HELP => 'Git move elements up',
	},

'Git undo' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => '000-u',
	HELP => 'Git undo',
	},

'Group selected elements' => 
	{
	GROUP_NAME => 'grouping leader',
	SHORTCUTS => '000-g',
	HELP => 'Group selected elements',
	},

'Help' => 
	{
	GROUP_NAME => 'commands leader',
	SHORTCUTS => '000-h',
	HELP => 'Help',
	},

'Import from clipboard to box' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '0AS-E',
	HELP => 'Import from clipboard to box',
	},

'Import from clipboard to text' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '0AS-T',
	HELP => 'Import from clipboard to text',
	},

'Import from primary to box' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  'C0S-V',
  '00S-P',
  'P'
],
	HELP => 'Import from primary to box',
	},

'Import from primary to text' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  '0A0-p',
  'A-P'
],
	HELP => 'Import from primary to text',
	},

'Insert' => 
	{
	GROUP_NAME => 'commands leader',
	SHORTCUTS => '000-r',
	HELP => 'Insert',
	},

'Insert from clipboard' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  'C00-v',
  '00S-Insert',
  'p'
],
	HELP => 'Insert from clipboard',
	},

'Insert leader' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-i',
	HELP => 'Insert leader',
	},

'Insert multi_wirl section' => 
	{
	GROUP_NAME => 'arrow leader',
	SHORTCUTS => '000-S',
	HELP => 'Insert multi_wirl section',
	},

'Load slides' => 
	{
	GROUP_NAME => 'slides leader',
	SHORTCUTS => '000-l',
	HELP => 'Load slides',
	},

'Make element narrower' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-1',
	HELP => 'Make element narrower',
	},

'Make element shorter' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-3',
	HELP => 'Make element shorter',
	},

'Make element taller' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-2',
	HELP => 'Make element taller',
	},

'Make element wider' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-4',
	HELP => 'Make element wider',
	},

'Make elements Unicode' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'C00-u',
	HELP => 'Make elements Unicode',
	},

'Make elements not Unicode' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'C0S-U',
	HELP => 'Make elements not Unicode',
	},

'Mouse drag canvas' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'C00-motion_notify',
	HELP => 'Mouse drag canvas',
	},

'Mouse duplicate elements' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  '0AS-button-press-1',
  '000-comma'
],
	HELP => 'Mouse duplicate elements',
	},

'Mouse emulation drag down' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '00S-Down',
	HELP => 'Mouse emulation drag down',
	},

'Mouse emulation drag left' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '00S-Left',
	HELP => 'Mouse emulation drag left',
	},

'Mouse emulation drag right' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '00S-Right',
	HELP => 'Mouse emulation drag right',
	},

'Mouse emulation drag up' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '00S-Up',
	HELP => 'Mouse emulation drag up',
	},

'Mouse emulation expand selection' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '00S-Odiaeresis',
	HELP => 'Mouse emulation expand selection',
	},

'Mouse emulation left-click' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-odiaeresis',
	HELP => 'Mouse emulation left-click',
	},

'Mouse emulation move down' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'C00-Down',
	HELP => 'Mouse emulation move down',
	},

'Mouse emulation move left' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'C00-Left',
	HELP => 'Mouse emulation move left',
	},

'Mouse emulation move right' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'C00-Right',
	HELP => 'Mouse emulation move right',
	},

'Mouse emulation move up' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'C00-Up',
	HELP => 'Mouse emulation move up',
	},

'Mouse emulation right-click' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-adiaeresis',
	HELP => 'Mouse emulation right-click',
	},

'Mouse emulation selection flip' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'C00-odiaeresis',
	HELP => 'Mouse emulation selection flip',
	},

'Mouse emulation toggle' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  '000-apostrophe',
  '\''
],
	HELP => 'Mouse emulation toggle',
	},

'Mouse expand selection' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '00S-button-press-1',
	HELP => 'Mouse expand selection',
	},

'Mouse left-click' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-button-press-1',
	HELP => 'Mouse left-click',
	},

'Mouse motion' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-motion_notify',
	HELP => 'Mouse motion',
	},

'Mouse motion 2' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '0AS-motion_notify',
	HELP => 'Mouse motion 2',
	},

'Mouse on element id' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-m',
	HELP => 'Mouse on element id',
	},

'Mouse quick box' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  'C0S-button-press-1'
],
	HELP => 'Mouse quick box',
	},

'Mouse quick link' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  '0A0-button-press-1',
  '000-period'
],
	HELP => 'Mouse quick link',
	},

'Mouse right-click' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-button-press-3',
	HELP => 'Mouse right-click',
	},

'Mouse selection flip' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'C00-button-press-1',
	HELP => 'Mouse selection flip',
	},

'Move selected elements down' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-Down',
	HELP => 'Move selected elements down',
	},

'Move selected elements down 2' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  '000-j',
  'j'
],
	HELP => 'Move selected elements down 2',
	},

'Move selected elements down quick' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '0A0-Down',
	HELP => 'Move selected elements down quick',
	},

'Move selected elements left' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-Left',
	HELP => 'Move selected elements left',
	},

'Move selected elements left 2' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  '000-h',
  'h'
],
	HELP => 'Move selected elements left 2',
	},

'Move selected elements left quick' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '0A0-Left',
	HELP => 'Move selected elements left quick',
	},

'Move selected elements right' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-Right',
	HELP => 'Move selected elements right',
	},

'Move selected elements right 2' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  '000-l',
  'l'
],
	HELP => 'Move selected elements right 2',
	},

'Move selected elements right quick' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '0A0-Right',
	HELP => 'Move selected elements right quick',
	},

'Move selected elements to the back' => 
	{
	GROUP_NAME => 'grouping leader',
	SHORTCUTS => '000-b',
	HELP => 'Move selected elements to the back',
	},

'Move selected elements to the front' => 
	{
	GROUP_NAME => 'grouping leader',
	SHORTCUTS => '000-f',
	HELP => 'Move selected elements to the front',
	},

'Move selected elements up' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-Up',
	HELP => 'Move selected elements up',
	},

'Move selected elements up 2' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  '000-k',
  'k'
],
	HELP => 'Move selected elements up 2',
	},

'Move selected elements up quick' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '0A0-Up',
	HELP => 'Move selected elements up quick',
	},

'Multi_wirl context_menu' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'mw_context_menu',
	HELP => 'Multi_wirl context_menu',
	},

'ORIGINS' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => undef,
	HELP => 'ORIGINS',
	},

'Open' => 
	{
	GROUP_NAME => 'commands leader',
	SHORTCUTS => '000-e',
	HELP => 'Open',
	},

'Prepend multi_wirl section' => 
	{
	GROUP_NAME => 'arrow leader',
	SHORTCUTS => '0A0-s',
	HELP => 'Prepend multi_wirl section',
	},

'Quick git' => 
	{
	GROUP_NAME => 'git',
	SHORTCUTS => [
  '000-button-press-3',
  '000-g'
],
	HELP => 'Quick git',
	},

'Quit' => 
	{
	GROUP_NAME => 'commands leader',
	SHORTCUTS => '000-q',
	HELP => 'Quit',
	},

'Quit no save' => 
	{
	GROUP_NAME => 'commands leader',
	SHORTCUTS => '00S-Q',
	HELP => 'Quit no save',
	},

'Redo' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  'C00-y',
  'C00-r'
],
	HELP => 'Redo',
	},

'Remove last section from multi_wirl' => 
	{
	GROUP_NAME => 'arrow leader',
	SHORTCUTS => 'C00-s',
	HELP => 'Remove last section from multi_wirl',
	},

'Remove rulers' => 
	{
	GROUP_NAME => 'change color/font leader',
	SHORTCUTS => '000-r',
	HELP => 'Remove rulers',
	},

'Ruler context_menu' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'ru_context_menu',
	HELP => 'Ruler context_menu',
	},

'Save' => 
	{
	GROUP_NAME => 'commands leader',
	SHORTCUTS => '000-w',
	HELP => 'Save',
	},

'SaveAs' => 
	{
	GROUP_NAME => 'commands leader',
	SHORTCUTS => '00S-W',
	HELP => 'SaveAs',
	},

'Select all elements' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  'C00-a',
  '00S-V'
],
	HELP => 'Select all elements',
	},

'Select connected elements' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-v',
	HELP => 'Select connected elements',
	},

'Select element by id' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '0A0-Tab',
	HELP => 'Select element by id',
	},

'Select elements by word' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'C00-f',
	HELP => 'Select elements by word',
	},

'Select elements by word no group' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'C0S-F',
	HELP => 'Select elements by word no group',
	},

'Select next arrow' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  'CA0-Tab',
  'C00-m'
],
	HELP => 'Select next arrow',
	},

'Select next element' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  '000-Tab',
  '000-n'
],
	HELP => 'Select next element',
	},

'Select next non arrow' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  'C00-Tab',
  'C00-n'
],
	HELP => 'Select next non arrow',
	},

'Select previous arrow' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  'CAS-ISO_Left_Tab',
  'C0S-M'
],
	HELP => 'Select previous arrow',
	},

'Select previous element' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  '00S-ISO_Left_Tab',
  '00S-N'
],
	HELP => 'Select previous element',
	},

'Select previous non arrow' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  'C0S-ISO_Left_Tab',
  'C0S-N'
],
	HELP => 'Select previous non arrow',
	},

'Shapes context_menu' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'sh_context_menu',
	HELP => 'Shapes context_menu',
	},

'Shrink box' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-s',
	HELP => 'Shrink box',
	},

'Temporary move to the front' => 
	{
	GROUP_NAME => 'grouping leader',
	SHORTCUTS => '00S-F',
	HELP => 'Temporary move to the front',
	},

'Test' => 
	{
	GROUP_NAME => 'debug leader',
	SHORTCUTS => '000-o',
	HELP => 'Test',
	},

'Undo' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  'C00-z',
  '000-u'
],
	HELP => 'Undo',
	},

'Ungroup selected elements' => 
	{
	GROUP_NAME => 'grouping leader',
	SHORTCUTS => '000-u',
	HELP => 'Ungroup selected elements',
	},

'Wirl arrow add section' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'CA0-button-press-1',
	HELP => 'Wirl arrow add section',
	},

'Wirl arrow insert flex point' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => 'CA0-button-press-2',
	HELP => 'Wirl arrow insert flex point',
	},

'Zoom in' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  '000-plus',
  'C00-j'
],
	HELP => 'Zoom in',
	},

'Zoom out' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => [
  '000-minus',
  'C00-h'
],
	HELP => 'Zoom out',
	},

'align leader' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '00S-A',
	HELP => 'align leader',
	},

'arrow end down' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '00S-Down',
	HELP => 'arrow end down',
	},

'arrow end down2' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '00S-J',
	HELP => 'arrow end down2',
	},

'arrow end left' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '00S-Left',
	HELP => 'arrow end left',
	},

'arrow end left2' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '00S-H',
	HELP => 'arrow end left2',
	},

'arrow end right' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '00S-Right',
	HELP => 'arrow end right',
	},

'arrow end right2' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '00S-L',
	HELP => 'arrow end right2',
	},

'arrow end up' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '00S-Up',
	HELP => 'arrow end up',
	},

'arrow end up2' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '00S-K',
	HELP => 'arrow end up2',
	},

'arrow leader' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-a',
	HELP => 'arrow leader',
	},

'arrow start down' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '000-Down',
	HELP => 'arrow start down',
	},

'arrow start down2' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '000-j',
	HELP => 'arrow start down2',
	},

'arrow start left' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '000-Left',
	HELP => 'arrow start left',
	},

'arrow start left2' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '000-h',
	HELP => 'arrow start left2',
	},

'arrow start right' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '000-Right',
	HELP => 'arrow start right',
	},

'arrow start right2' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '000-l',
	HELP => 'arrow start right2',
	},

'arrow start up' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '000-Up',
	HELP => 'arrow start up',
	},

'arrow start up2' => 
	{
	GROUP_NAME => 'move arrow ends',
	SHORTCUTS => '000-k',
	HELP => 'arrow start up2',
	},

'change color/font leader' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-z',
	HELP => 'change color/font leader',
	},

'clone' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-c',
	HELP => 'clone',
	},

'clone angled arrow' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => '00S-A',
	HELP => 'clone angled arrow',
	},

'clone arrow' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => '000-a',
	HELP => 'clone arrow',
	},

'clone box' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => '000-b',
	HELP => 'clone box',
	},

'clone down' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => '000-Down',
	HELP => 'clone down',
	},

'clone emulation down' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => 'C00-Down',
	HELP => 'clone emulation down',
	},

'clone emulation left' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => 'C00-Left',
	HELP => 'clone emulation left',
	},

'clone emulation right' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => 'C00-Right',
	HELP => 'clone emulation right',
	},

'clone emulation up' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => 'C00-Up',
	HELP => 'clone emulation up',
	},

'clone escape' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => '000-Escape',
	HELP => 'clone escape',
	},

'clone flip hint lines' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => '000-h',
	HELP => 'clone flip hint lines',
	},

'clone insert' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => '000-button-press-1',
	HELP => 'clone insert',
	},

'clone insert2' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => '000-Return',
	HELP => 'clone insert2',
	},

'clone left' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => '000-Left',
	HELP => 'clone left',
	},

'clone motion' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => '000-motion_notify',
	HELP => 'clone motion',
	},

'clone right' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => '000-Right',
	HELP => 'clone right',
	},

'clone text' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => '000-t',
	HELP => 'clone text',
	},

'clone up' => 
	{
	GROUP_NAME => 'clone',
	SHORTCUTS => '000-Up',
	HELP => 'clone up',
	},

'commands leader' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '00S-colon',
	HELP => 'commands leader',
	},

'create one stripe group' => 
	{
	GROUP_NAME => 'stripes leader',
	SHORTCUTS => '000-1',
	HELP => 'create one stripe group',
	},

'create stripes group' => 
	{
	GROUP_NAME => 'stripes leader',
	SHORTCUTS => '000-g',
	HELP => 'create stripes group',
	},

'debug leader' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '00S-D',
	HELP => 'debug leader',
	},

'first slide' => 
	{
	GROUP_NAME => 'slides leader',
	SHORTCUTS => '000-g',
	HELP => 'first slide',
	},

'flip cross mode' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-x',
	HELP => 'flip cross mode',
	},

'git' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '00S-G',
	HELP => 'git',
	},

'grouping leader' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '000-g',
	HELP => 'grouping leader',
	},

'move arrow ends' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '0A0-a',
	HELP => 'move arrow ends',
	},

'next slide' => 
	{
	GROUP_NAME => 'slides leader',
	SHORTCUTS => '000-n',
	HELP => 'next slide',
	},

'previous slide' => 
	{
	GROUP_NAME => 'slides leader',
	SHORTCUTS => '00S-N',
	HELP => 'previous slide',
	},

'slides leader' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '00S-S',
	HELP => 'slides leader',
	},

'stripes leader' => 
	{
	GROUP_NAME => '',
	SHORTCUTS => '0A0-g',
	HELP => 'stripes leader',
	},

'ungroup stripes group' => 
	{
	GROUP_NAME => 'stripes leader',
	SHORTCUTS => '000-u',
	HELP => 'ungroup stripes group',
	},
}

